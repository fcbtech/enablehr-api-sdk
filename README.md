# API endpoints

These endpoints allow direct updates of payroll data within enableHR.
| Method | Path |  Description |
|-------------|-------|--------------------------------------------|
| POST | `/api/v0/login` | Obtain an access token |
| GET | `/api/v0/account/branches` | List all branches that this user is permitted to see |
| GET | `/api/v0/employees` | List all employees that this user is permitted to see |
| POST | `/api/v0/employees` | Create an employee |
| GET | `/api/v0/employees/{id}` | Retrieve a specific employee |
| PUT | `/api/v0/employees/{id}` | Update an employee |
| DELETE | `/api/v0/employees/{id}` | Delete an employee |
| GET | `/api/v0/employees/{id}/bank-details` | Retrieve the bank details for a specific employee |
| PUT | `/api/v0/employees/{id}/bank-details` | Create or update bank details for a specific employee |
| DELETE | `/api/v0/employees/{id}/bank-details` | Delete bank details for a specific employee |
| GET | `/api/v0/employees/{id}/tax-details` | Retrieve the tax details for a specific employee |
| PUT | `/api/v0/employees/{id}/tax-details` | Create or update tax details for a specific employee |
| DELETE | `/api/v0/employees/{id}/tax-details` | Delete tax details for a specific employee |
| GET | `/api/v0/employees/{id}/superannuation-details` | Retrieve the super details for a specific employee |
| PUT | `/api/v0/employees/{id}/superannuation-details` | Create or update super details for a specific employee |
| DELETE | `/api/v0/employees/{id}/superannuation-details` | Delete super details for a specific employee |
| GET | `/api/v0/employees/{id}/leave-balance` | Retrieve the leave details for a specific employee |
| PUT | `/api/v0/employees/{id}/leave-balance` | Create or update leave details for a specific employee |
| DELETE | `/api/v0/employees/{id}/leave-balance` | Delete leave details for a specific employee |

___

## Authentication
POST `api/v0/login` 
Obtain an access token

### Required headers
We require you to provide a user agent for debug purposes. We suggest you use the name of your organization, or a technical contact within it.
```
"Content Type": "application/json", "User Agent": "{Name of your API agent}"
```
**Parameters**
You must provide basic authentication details.
|          Name | Required |  Type   | Description |
| -------------|--------|-------|------------|
|     `username` | required | string  | Simple auth username |
|     `password` | required | string  | Simple auth password |

**Response**
Represents the details of the logged-in user.
```
{
"id": "",
"username": "", "name": "", "email": ""
}
```

### Response header
The API access token for use in subsequent requests. You will need to cache this value.
```
token: {API_ACCESS_TOKEN}
```
Note: A token is valid for two hours from issue.