import assert from "assert";
import config from "../config.json";

import jsonRequest from "../src/json-request";

const user = config.apiUser;

const employee = {
    "firstName": "Danny",
    "lastName": "DeVito",
    "businessEmail": "danny@devito.com.au",
    "mobile": "0404 044 044",
    "telephone": "02 1234 1234",
    "gender": "Male",
    "employeeType": "Employee",
    "employmentType": "Permanent_FullTime",
    "externalId": "EE4545",
    "employeeStatus": "Current",
    "branches": [
        {
            "id": config.branchId
        }
    ],
    "payrollNumber": "EE4545"
};

const bankAccounts = [{
    "accountName": "My Account 1",
    "bsb": 123456,
    "accountNumber": 123456,
    "statementText": "text",
    "accountOrder": 0,
    "dollarAmount": null,
    "remainder": true
}, {
    "accountName": "My Account 2",
    "bsb": 123456,
    "accountNumber": 123456,
    "statementText": "text",
    "accountOrder": 1,
    "dollarAmount": 100,
    "remainder": false
}];

const exemptTaxCodes = {
    '000000000': 'UNQUOTED',
    '111111111': 'PENDING',
    '333333333': 'UNDER18',
    '444444444': 'PENSIONER',
};

const taxDetails = {
    "taxFileNumber": 123456782,
    "taxFreeThresholdClaimed": false,
    "tradeSupportLoan": false,
    "helpDebt": false,
    "studentStartupLoan": false,
    "sfssDebt": false,
    "residencyStatus": "AUSTRALIAN_RESIDENT"
};

const superannuation = [{
    "fundType": "REGULATED",
    "fundName": "My Super fund",
    "abn": 12345678901,
    "electronicServiceAddress": "123 Super St",
    "uniqueSuperFundId": "My Super fund Id",
    "employeeNumber": "Employer number"
}];

const leaveDetails = {
    "annualLeave": 1,
    "personalLeave": 2,
    "longServiceLeave": 3
};

const testAsUserWithReadWriteCreate = () => {
    describe(user.username, function () {
        let token;
        let createdEmployeeId;
        it("can retrieve an auth token", function (done) {
            this.timeout(0);
            jsonRequest('POST', user, null, '/login', function (response, body) {
                token = response.headers['token'];
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });
        it("branches list is not empty", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/account/branches', function (response, body) {
                assert.strictEqual(200, response.statusCode);
                assert(body.length > 0);
                done();
            });
        });
        it("can create an employee", function (done) {
            this.timeout(0);
            var clone = JSON.parse(JSON.stringify(employee));
            jsonRequest('POST', clone, token, '/employees', function (response, body) {
                console.log(body);
                createdEmployeeId = body.id;
                assert.strictEqual(201, response.statusCode);
                done();
            });
        });
        it("can delete an employee", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId, function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });
    });
};

const testBankAccount = () => {
    describe('Test bank account using ' + user.username, function () {
        let token;
        let createdEmployeeId;
        let clone = JSON.parse(JSON.stringify(employee));
        let cloneBank = JSON.parse(JSON.stringify(bankAccounts));
        it("obtain a token", function (done) {
            this.timeout(0);
            jsonRequest('POST', user, null, '/login', function (response, body) {
                token = response.headers['token'];
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });
        it("create an employee", function (done) {
            this.timeout(0);
            jsonRequest('POST', clone, token, '/employees', function (response, body) {
                createdEmployeeId = body.id;
                assert.strictEqual(201, response.statusCode);
                done();
            });
        });

        it("can retrieve bank details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/bank-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can update bank details", function (done) {
            this.timeout(0);
            jsonRequest('PUT', cloneBank, token, '/employees/' + createdEmployeeId + '/bank-details', function (response, body) {
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can retrieve updated bank details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/bank-details', function (response, body) {
                console.log(JSON.stringify(body));
                assert(body.length != 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can delete bank details", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId + '/bank-details', function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });

        it("can retrieve empty bank details after delete", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/bank-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("delete the employee", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId, function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });
    });
};

const testTaxDetails = () => {
    describe('Test tax details account using ' + user.username, function () {
        let token;
        let createdEmployeeId;
        let clone = JSON.parse(JSON.stringify(employee));
        it("obtain a token", function (done) {
            this.timeout(0);
            jsonRequest('POST', user, null, '/login', function (response, body) {
                token = response.headers['token'];
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("create an employee", function (done) {
            this.timeout(0);
            jsonRequest('POST', clone, token, '/employees', function (response, body) {
                createdEmployeeId = body.id;
                assert.strictEqual(201, response.statusCode);
                done();
            });
        });

        it("can retrieve tax details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(404, response.statusCode);
                done();
            });
        });

        it("can update tax details", function (done) {
            this.timeout(0);
            jsonRequest('PUT', taxDetails, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can retrieve updated tax details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                console.log(JSON.stringify(body));
                assert(body.length != 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can delete tax details", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });

        it("can retrieve empty tax details after delete", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(404, response.statusCode);
                done();
            });
        });

        it("delete the employee", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId, function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });
    });
};

const testExemptTaxCodes = () => {
    describe('Test exempt tax codes using ' + user.username, function () {
        let token;
        let createdEmployeeId;
        let branchId;
        let clone = JSON.parse(JSON.stringify(employee));
        it("obtain a token", function (done) {
            this.timeout(0);
            jsonRequest('POST', user, null, '/login', function (response, body) {
                token = response.headers['token'];
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("obtain a valid branch from employee list", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees', function (response, body) {
                assert.strictEqual(200, response.statusCode);
                assert(body.length > 0);
                branchId = body[0].branches[0].id;
                clone.branches[0].id = branchId;
                done();
            });
        });

        it("create an employee", function (done) {
            this.timeout(0);
            jsonRequest('POST', clone, token, '/employees', function (response, body) {
                createdEmployeeId = body.id;
                assert.strictEqual(201, response.statusCode);
                done();
            });
        });

        it("can retrieve tax details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(404, response.statusCode);
                done();
            });
        });

        Object.keys(exemptTaxCodes).forEach(function(code) {
            const value = exemptTaxCodes[code];
            it("Can use exempt code " + code + " (" + value + ")", function (done) {
                let payload = JSON.parse(JSON.stringify(taxDetails));
                payload.taxFileNumber = code;
                this.timeout(0);
                jsonRequest('PUT', payload, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                    assert.strictEqual(200, response.statusCode);
                    done();
                });
            });

            it("Verify code " + code + " (" + value + ")", function (done) {
                this.timeout(0);
                jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/tax-details', function (response, body) {
                    console.log(JSON.stringify(body));
                    assert(!body.taxFileNumber);
                    assert(body.exemptType == value);
                    assert.strictEqual(200, response.statusCode);
                    done();
                });
            });
        });

        it("delete the employee", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId, function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });
    });
};

const testSuperannuation = () => {
    describe('Test superannuation account using ' + user.username, function () {
        let token;
        let createdEmployeeId;
        let clone = JSON.parse(JSON.stringify(employee));
        let cloneBank = [JSON.parse(JSON.stringify(bankAccounts))[0]];
        let cloneSuper = JSON.parse(JSON.stringify(superannuation));
        it("obtain a token", function (done) {
            this.timeout(0);
            jsonRequest('POST', user, null, '/login', function (response, body) {
                token = response.headers['token'];
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });
        it("create an employee", function (done) {
            this.timeout(0);
            jsonRequest('POST', clone, token, '/employees', function (response, body) {
                createdEmployeeId = body.id;
                assert.strictEqual(201, response.statusCode);
                done();
            });
        });

        it("can retrieve superannuation details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/superannuation-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can update superannuation details", function (done) {
            this.timeout(0);
            jsonRequest('PUT', cloneSuper, token, '/employees/' + createdEmployeeId + '/superannuation-details', function (response, body) {
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can retrieve updated superannuation details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/superannuation-details', function (response, body) {
                console.log(JSON.stringify(body));
                assert(body.length != 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can delete superannuation details", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId + '/superannuation-details', function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });

        it("can retrieve empty superannuation details after delete", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/superannuation-details', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("delete the employee", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId, function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });
    });
};

const testLeaveBalance = () => {
    describe('Test leave details account using ' + user.username, function () {
        let token;
        let createdEmployeeId;
        let clone = JSON.parse(JSON.stringify(employee));
        it("obtain a token", function (done) {
            this.timeout(0);
            jsonRequest('POST', user, null, '/login', function (response, body) {
                token = response.headers['token'];
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("create an employee", function (done) {
            this.timeout(0);
            jsonRequest('POST', clone, token, '/employees', function (response, body) {
                createdEmployeeId = body.id;
                assert.strictEqual(201, response.statusCode);
                done();
            });
        });

        it("can retrieve leave details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/leave-balance', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(404, response.statusCode);
                done();
            });
        });

        it("can update leave details", function (done) {
            this.timeout(0);
            jsonRequest('PUT', leaveDetails, token, '/employees/' + createdEmployeeId + '/leave-balance', function (response, body) {
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can retrieve updated leave details", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/leave-balance', function (response, body) {
                console.log(JSON.stringify(body));
                assert(body.length != 0);
                assert.strictEqual(200, response.statusCode);
                done();
            });
        });

        it("can delete leave details", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId + '/leave-balance', function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });

        it("can retrieve empty leave details after delete", function (done) {
            this.timeout(0);
            jsonRequest('GET', null, token, '/employees/' + createdEmployeeId + '/leave-balance', function (response, body) {
                assert(body.length == 0);
                assert.strictEqual(404, response.statusCode);
                done();
            });
        });

        it("delete the employee", function (done) {
            this.timeout(0);
            jsonRequest('DELETE', null, token, '/employees/' + createdEmployeeId, function (response, body) {
                assert.strictEqual(204, response.statusCode);
                done();
            });
        });
    });
};

testAsUserWithReadWriteCreate();
testBankAccount();
testTaxDetails();
testExemptTaxCodes();
testSuperannuation();
testLeaveBalance();
