const http = require("http");
const https = require("https");

import config from "../config.json";

export default function (method, data, authorization, endpoint, onResult) {
    const transport = config.secure ? https : http;
    const body = data ? JSON.stringify(data) : null;
    let options = {
        host: config.host,
        port: config.port,
        path: config.apiBase + endpoint,
        method,
        headers: {
            "Content-Type": "application/json",
            "User-Agent": "enableHR API expressJS client"
        }
    };
    if (authorization) {
        if (authorization.hasOwnProperty('username')) {
            options.headers['Authorization'] = 'Basic ' + Buffer.from(authorization.username + ':' + authorization.password).toString('base64');
        } else {
            options.headers['Authorization'] = 'Bearer ' + authorization;
        }
    }
    if (body) {
        options.headers["Content-Length"] = Buffer.byteLength(body);
    }
    var req = transport.request(options, function (res) {
        var output = '';
        console.log(method + " " + options.host + options.path + ':' + options.port + ' => ' + res.statusCode);
        res.setEncoding('utf8');

        res.on('data', function (chunk) {
            output += chunk;
        });

        res.on('end', function () {
            try {
                var obj = JSON.parse(output);
            } catch (e) {
                onResult(res, output);
                return;
            }
            onResult(res, obj);
        });

        req.on('error', function (err) {
            res.send('error: ' + err.message);
        });
    });
    if (body) {
        req.write(body);
    }
    req.end();
};